export const METADATA = {
  INDEX: {
    title: 'OCG',
    description: 'Open Consulting Group',
    keywords: 'OCG, Artificial Intelligenge, Blockchain, IoT, crypto',
  },
  ABOUT: {
    title: 'About Us | OCG',
    description: 'About us',
    keywords: 'OCG, Blockchain, IoT, crypto, Artificial Intelligenge',
  }
}