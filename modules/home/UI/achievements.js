import { Container, Col, Row } from 'reactstrap';
import Img from '../../../components/ImageHolder/imageHolder';

const BUSINESS = './../../../static/assets/achievements/business.png';
const TIME = './../../../static/assets/achievements/time.png';
const INSIGHTS = './../../../static/assets/achievements/insight.png';
const PROJECT = './../../../static/assets/achievements/projects.png';
const SATISFACTION = './../../../static/assets/achievements/satisfaction.png';

const ACHIEVEMENTS = [{
  icon: BUSINESS,
  number: '150+',
  content: 'Businesses Consulted',
}, {
  icon: TIME,
  number: '27%',
  content: 'Average Supply Chain Time Optimization',
}, {
  icon: INSIGHTS,
  number: '5k+',
  content: 'Business Insights Generated',
}, {
  icon: PROJECT,
  number: '19',
  content: 'Blockchain Projects deployed',
}, {
  icon: SATISFACTION,
  number: '97%',
  content: 'Average Client Satisfaction Index',
}];

export default () => {
  return (
    <div className="achievement-container">
      <Container>
        <Row>
          <Col lg="12" className="content">
            <h1 className="title">
              Our achievements, so far
            </h1>
          </Col>
        </Row>
        <Row>
          <Col lg="12">
            <div className="achievements">
              {ACHIEVEMENTS.map((section, index) => {
                const { icon, number, content } = section;
                return (
                  <div className="ticket" key={index}>
                    <div className="image-container">
                      <Img className="image" src={icon}/>
                    </div>
                    <h4 className="number">{number}</h4>
                    <h6 className="content">{content}</h6>
                  </div>
                )
              })}
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  )
}