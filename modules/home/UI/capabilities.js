import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import Img from '../../../components/ImageHolder/imageHolder';
import Swiper from 'react-id-swiper';
import 'swiper/css/swiper.css';
import Ecommerce from './../../../static/assets/ecommerce.png';
import Finance from './../../../static/assets/finance.png';
import Blockchain from './../../../static/assets/blockchain.png';
import SupplyChain from './../../../static/assets/supplyChain.png';

export default () => {
  const params = {
    slidesPerView: 'auto',
    spaceBetween: 30,
    // loop: true,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    }
  }

  return (
    <div className="capabilities-container">
      <Container>
        <Row>
          <Col lg="12">
            <h1 className="heading">
              Know more about our capabilities
            </h1>
            <Swiper {...params}>
              {Capabilities.map((point, index) => {
                const { image, title, description } = point;
                return (
                  <div key={index} className="capability-slide">
                    <div className="slide-image-container">
                      <Img className="slide-image" alt="capability slide" src={image} />
                    </div>
                    <div className="content">
                      <div className="title">{title}</div>
                      <div className="description">{description}</div>
                    </div>
                  </div>
                )
              })}
            </Swiper>
          </Col>
        </Row>
      </Container>
    </div>
  )
}

const Capabilities = [{
  image: Ecommerce,
  title: 'Ecommerce',
  description: 'Effective user engagement and conversion through tested and proven customer journey mapping',
}, {
  image: Finance,
  title: 'Finance',
  description: 'Make your system efficient and fraud-proof through automation',
}, {
  image: Blockchain,
  title: 'Blockchain',
  description: 'Pioneering in bleeding edge distributed ledger technology for the next generation of decentralized autonomous organizations',
}, {
  image: SupplyChain,
  title: 'Supply Chain',
  description: 'Track, verify and optimize each aspect of the supply chain through machine learning and artificial intelligence enable automated solutions',
}]