import { Container, Col, Row } from 'reactstrap';
import Img from '../../../components/ImageHolder/imageHolder';
import Fast from './../../../static/assets/fast.png';
import Accurate from './../../../static/assets/smart2.png';
import Agnostic from './../../../static/assets/relevant.png';
import Intuitive from './../../../static/assets/intuitive.png';
import Efficient from './../../../static/assets/efficient.png';
import Scalable from './../../../static/assets/scalable.png';

export default () => (
  <div className="empower-section">
    <div className="desktop-view">
      <h1 className="title">
        Empower your business with OCG
      </h1>
      <div className="empower-points-container">
        <div className="empower-points">
          {empowerPoints.map((dataSet, index) => {
            return (
              <div className="dataset" key={index}>
                {dataSet.map((point, pointIndex) => {
                  const { icon, title, description, iconClass } = point;
                  return (
                    <div className="point-wrapper" key={pointIndex}>
                      <div key={pointIndex} className="point-container">
                        <div className="icon">
                          <Img className={`empower-icon ${iconClass}`} alt="empower icon" src={icon} />
                        </div>
                        <div className="empower-title">{title}</div>
                        <div className="empower-description">{description}</div>
                      </div>
                    </div>
                  )
                })}
              </div>
            )
          })}
        </div>
      </div>
    </div>
    <div className="mobile-view">
      <div className="mobile-empower-points-container">
        <div className="mobile-empower-points">
          {mobileEmpowerPoints.map((point, index) => {
            const { title, description, icon, iconClass } = point;
            return (
              <div className="point-container" key={index}>
                <div className="icon-section">
                  <Img className={`empower-icon ${iconClass}`} alt="empower icon" src={icon} />
                </div>
                <div className="content">
                  <div className="empower-title">{title}</div>
                  <div className="empower-description">{description}</div>
                </div>
              </div>
            )
          })}
        </div>
      </div>
    </div>
  </div>
)

const dataSetOne = [{
  icon: Fast,
  title: 'Fast',
  iconClass: 'fast-icon',
  description: 'Need for minimal human intervention',
}, {
  icon: Accurate,
  title: 'Reconnaissance',
  description: 'In depth research to provide most optimum solutions',
}, {
  icon: Agnostic,
  title: 'Domain agnostic',
  description: 'Solutions to practically every business',
}];

const dataSetTwo = [{
  icon: Intuitive,
  title: 'Intuitive',
  description: 'No learning curve for both businesses and their customers',
}, {
  icon: Efficient,
  title: 'Goals Oriented',
  description: 'Measurable goals through analytical business funnel insights',
}, {
  icon: Scalable,
  title: 'Scalable',
  description: 'Don’t just optimise, grow. OCG succeeds with your success',
}];

const empowerPoints = [dataSetOne, dataSetTwo];

const mobileEmpowerPoints = dataSetOne.concat(dataSetTwo);
