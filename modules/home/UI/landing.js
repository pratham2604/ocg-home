import React, { Component } from 'react';
import { Container, Col, Row } from 'reactstrap';
import Img from '../../../components/ImageHolder/imageHolder';
const LANDING_IMAGE =  './../../../static/assets/homepage.gif';
const LOGO = './../../../static/assets/OCG_BLUE.svg';

export default () => {
  return (
    <div className="landing-container">
      <Container>
        <Row className="section landing">
          <Col lg="4" md="12" className="content">
            <div>
              <Img className="logo" alt="logo" src={LOGO} />
            </div>
            <h1 className="title">
              We create <span className="highlight">technology</span> that matters
            </h1>
            <p className="description">
              Technology is the future and critical for any successful business.
               OCG empowers businesses with not only business optimisation consultation but also the technical 
               development required to execute the proposed solutions
            </p>
            <a href="mailto:junaid@ocg.technology" className="button-animation">
              Contact Us
            </a>
          </Col>
          <Col lg="8" className="landing-image-container">
            <Img className="landing-image" alt="landing-image" src={LANDING_IMAGE} />
          </Col>
        </Row>
      </Container>
    </div>
  )
}