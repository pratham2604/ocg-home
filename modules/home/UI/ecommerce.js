import { Container, Col, Row } from 'reactstrap';
import Img from '../../../components/ImageHolder/imageHolder';
import Link from 'next/link';
const ECOMMERCE_IMAGE =  './../../../static/assets/ecommerce.png';

export default () => {
  return (
    <div className="ecommerce-container">
      <Container>
        <Row>
          <Col lg="6">
            <Img className="ecommerce-image" alt="landing-image" src={ECOMMERCE_IMAGE} />
          </Col>
          <Col lg="6" className="content">
            <div className="tag">
              ECOMMERCE SOLUTION
            </div>
            <h2 className="title">
              Excel in ECommerce with us
            </h2>
            <h5 className="description">
              Open Consulting Group is a multi-channel ecommerce solution, 
              which enables you to sell globally on your own website and top marketplaces like Amazon, 
              Ebay, Etsy from a single interface. We also provide solution for Blockchain, Artificial Intelligenge
              and IoT.
            </h5>
            <div className="link-container">
              <Link href="/">
                <a className="link">Grow your Business Online</a>
              </Link>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  )
}