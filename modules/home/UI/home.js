import Landing from './landing';
import Achievements from './achievements';
import Ecommerce from './ecommerce';
import Empower from './empower';
import Capabilities from './capabilities';
import '../styles/_home.scss';

export default () => (
  <div className="home">
    <Landing />
    <Empower />
    <Capabilities />
    <Achievements />
  </div>
)