import { Container, Col, Row } from 'reactstrap';
import Img from '../../../components/ImageHolder/imageHolder';
import '../styles/_about.scss';
const LOGO = './../../../static/assets/OCG_BLUE.svg';
const ABOUT = './../../../static/assets/about/about.png';
const WHO = './../../../static/assets/about/who.png';
const WHAT = './../../../static/assets/about/what.png';
const WHY = './../../../static/assets/about/why.png';

export default () => (
  <div className="about-container">
    <Container>
      <Row className="section about">
        <Col lg="12" md="12">
          <div>
            <Img className="logo" alt="logo" src={LOGO} />
          </div>
          <div className="landing-section">
            <div className="heading">
              <h1 className="title">Open Consulting Group</h1>
              <span className="vertical-text">About OCG</span>
            </div>
            <div className="content">
              <Img className="image" alt="about" src={ABOUT} />
              <div className="info">
                A Truly Decentralized Autonomous Organization
              </div>
            </div>
          </div>
          <div className="who-section">
            <div className="image-container">
              <span className="number">1</span>
              <Img className="image" alt="about" src={WHO} />
              <span className="vertical-text">Who ?</span>
            </div>
            <div className="info">
              <div className="title">Who are we?</div>
              <div className="content">
                We are a dedicated group of tech artists who believe in excellence and ownership. 
                We place utmost importance on quality. For us, customer delight is a top priority.
              </div>
            </div>
          </div>
          <div className="what-section">
            <div className="info">
              <div className="title">What do we do?</div>
              <div className="content">
                We help businesses to identify risks, opportunities and growth from data. 
                We do this with our deep Reconnaissance and HCI (Human Computer Interaction) expertise. 
                OCG can help your business scale seamlessly by optimising processes and infusing technology.
              </div>
            </div>
            <div className="image-container">
              <span className="vertical-text">What ?</span>
              <Img className="image" alt="about" src={WHAT} />
              <span className="number">2</span>
            </div>
          </div>
          <div className="who-section">
            <div className="image-container">
              <span className="number">3</span>
              <Img className="image" alt="about" src={WHY} />
              <span className="vertical-text">Why ?</span>
            </div>
            <div className="info">
              <div className="title">Why do we do it?</div>
              <div className="content">
                For us, technology is not only a tool but a source to unleash the untapped potential 
                through the generated insights. With current tech-overload, constructing a clear and 
                easy to use solution is a challenge. Our goal is to make technology simplified 
                and user-relevant with our unique customer centric engineering approach.
              </div>
            </div>
          </div>
        </Col>
      </Row>
    </Container>
  </div>
)