import Layout from '../components/Layout/Layout';
import About from '../modules/about/UI/about';
import { METADATA } from '../constants/metadata';

export default () => {
  const { description, keywords, title } = METADATA.ABOUT;

  return (
    <Layout pageTitle={title} description={description} keywords={keywords}>
      <About />
    </Layout>
  );
}