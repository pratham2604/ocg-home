import Layout from '../components/Layout/Layout';
import Home from '../modules/home/UI/home';
import { METADATA } from '../constants/metadata';
import 'bootstrap/dist/css/bootstrap.min.css';

export default () => {
  const { description, keywords, title } = METADATA.INDEX;

  return (
    <Layout pageTitle={title} description={description} keywords={keywords}>
      <Home />
    </Layout>
  );
}