import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import { FOOTER_MENU } from './menu';
import Link from 'next/link';
import Img from './../ImageHolder/imageHolder';
import LOGO from '../../static/assets/OCG_White.svg'
import './footer.scss';

export default class Footer extends Component {
  render() {
    return (
      <div className="footer">
        <Container>
          <Row>
            <Col md="3" sm="6" className="logo-section">
              <Img className="logo" alt="logo" src={LOGO} />
              <div>
                <p className="copyrights ibmplex-font">© 2020 deKrypt. All rights Reserved.</p>
              </div>
            </Col>
            {FOOTER_MENU.map((section, index) => {
              const { title, list } = section;
              return (
                <Col md="3" sm="6" key={index}>
                  <div className="menu-title">
                    {title}
                  </div>
                  <div className="menu-list">
                    {list.map((item, itemIndex) => {
                      const { link, label } = item;
                      return (
                        <Link key={itemIndex} href={link}>
                          <a className="menu-item">{label}</a>
                        </Link>
                      )
                    })}
                  </div>
                </Col>
              )
            })}
            <Col md="3" sm="6">
              <div className="menu-title">
                Contact Us
              </div>
              <a href="mailto:hello@ocg.technology" className="mail-link">hello@ocg.technology</a>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}