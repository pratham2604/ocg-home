export const FOOTER_MENU = [{
  title: 'Company',
  list: [{
    link: '/about',
    label: 'About',
  }, {
    link: '/',
    label: 'Careers (coming soon)',
  }, {
    link: '/',
    label: 'Request Demo (coming soon)',
  }]
}, {
  title: 'Industry',
  list: [{
    link: '/',
    label: ' SAAS (coming soon)',
  }, {
    link: '/',
    label: 'Blog (coming soon)',
  }]
}]