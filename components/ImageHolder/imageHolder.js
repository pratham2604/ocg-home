import React, { Fragment, Component } from 'react';
import Loader from '../Loader/loader';

export default class extends Component {
  state = {
    loadImage: false,
  }

  componentDidMount() {
    this.setState({
      loadImage: true,
    });
  }

  render() {
    const { loadImage } = this.state;
    const { className, alt, src } = this.props;

    return (
      <Fragment>
        {loadImage ?
          <img className={className} alt={alt} src={src} /> :
          <Loader />
        }
      </Fragment>
    )
  }
}