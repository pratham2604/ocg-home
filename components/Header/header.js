import React, { Component } from 'react';
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem } from 'reactstrap';
import LOGO from '../../static/assets/OCG_White.svg'
import Link from 'next/link';
import { MENU } from './menu';
import './header.scss';

export default class extends Component {
  state = {
    isCollapsed: false,
  };

  toggle = () => {
    this.setState((state) => {
      const { isCollapsed } = state;
      return {
        isCollapsed: !isCollapsed,
      };
    });
  }

  render() {
    const { isCollapsed } = this.state;

    return (
      <Navbar dark expand="lg" sticky={'top'} className="header" id="global-header">
        <div className="container navigation-conatiner">
          <NavbarBrand>
            <Link href="/">
              <img className="icon-hbits-logo logo" src={LOGO}></img>
            </Link>
          </NavbarBrand>
          {/* <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={isCollapsed} navbar>
            <Nav className="ml-auto" navbar>
              {MENU.map((menu) => {
                const { id, label, link } = menu;
                return (
                  <NavItem className="menu poppins-font" key={id}>
                    <Link href={link}>
                      <a className="link">{label}</a>
                    </Link>
                  </NavItem>
                );
              })}
            </Nav>
          </Collapse> */}
        </div>
      </Navbar>
    );
  }
}