import React, { Component } from 'react';
import Header from '../Header/header';
import Head from 'next/head';
import favicon from '../../static/favicon.ico';
import Footer from '../Footer/footer';
import './Layout.scss';
// import './../../constants/css3-animate-it';
// import 

export default class Layout extends Component {
  render() {
    const {
      pageTitle = 'dekrypt',
      description = '',
      keywords = '',
    } = this.props;

    return (
      <div className="layout">
        <Head>
          <title>{pageTitle}</title>
          <meta name="description" content={description}></meta>
          <meta name="keywords" content={keywords}></meta>
          <meta name="viewport" content="initial-scale=1.0, width=device-width" />
          <link rel="icon" href={favicon} sizes="16x16 32x32" type="image/png"></link>
          <link href="/static/assets/animations.css"></link>
          <link href="https://fonts.googleapis.com/css?family=Ubuntu:400,500,700&display=swap" rel="stylesheet"></link>
        </Head>
        {/* <Header /> */}
        {this.props.children}
        <Footer />
      </div>
    );
  }
}